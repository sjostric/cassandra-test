package com.tieto.pds.swdi.cassandra;

import java.util.List;
import java.util.UUID;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.Host;
import com.datastax.driver.core.Metadata;
import com.datastax.driver.core.Session;
import com.tieto.pds.swdi.cassandra.domain.Department;
import com.tieto.pds.swdi.cassandra.repository.DepartmentRepository;

public class SimpleClient {

	private Cluster cluster;
	private Session session;

	public static void main(String[] args) {
		String host;
		if (args.length > 0) {
			host = args[1];
		} else {
			host = "127.0.0.1";
		}
		System.out.println("Hello Cassandra Departments");
		SimpleClient sc = new SimpleClient();
		sc.work(host);
		System.out.println("Bye Cassandra Departments");
		System.exit(0);
	}

	void work(String host) {
		connect(host);
		boolean createData = true;
		boolean printData = true;

		if (createData) {
			dropKeyspace("hrdb");
			createSchema();
			System.out.println("START: creating data");
			loadData(2, 10);
			System.out.println("DONE: creating data");
		}

		DepartmentRepository depRepo = new DepartmentRepository(session);
		System.out.println("START: Fetching the data");

		long startTime = System.currentTimeMillis();
		List<Department> departments = depRepo.getDepartments();
		int totalTime = (int) ((System.currentTimeMillis() - startTime) / 1000);
		System.out.println("DONE: Fetching the data: "
				+ Integer.toString(totalTime));
		for (Department department : departments) {
			if (printData) {
				System.out.println(department.toString());
			}
		}
		session.close();

	}

	private void dropKeyspace(String keyspace) {
		session.execute("DROP KEYSPACE IF EXISTS " + keyspace + ";");
	}

	private void loadData(int numDepartments, int numUsersPerDepartment) {

		for (int i = 0; i < numDepartments; i++) {

			String department_id = UUID.randomUUID().toString()
					.replaceAll("-", "");
			String depName = "Department" + Integer.toString(i);
			session.execute("INSERT INTO hrdb.department (id, name) "
					+ "VALUES ('" + department_id + "'," + "'" + depName
					+ "');");

			for (int j = 0; j < numUsersPerDepartment; j++) {

				String employee_id = UUID.randomUUID().toString()
						.replaceAll("-", "");
				String name = "John" + Integer.toString(j);
				session.execute("INSERT INTO hrdb.employee (id, department_id, name) "
						+ "VALUES ('"
						+ employee_id
						+ "', '"
						+ department_id
						+ "', '" + name + "');");

			}
		}
	}

	private void connect(String node) {
		cluster = Cluster.builder().addContactPoint(node).build();
		Metadata metadata = cluster.getMetadata();
		System.out.printf("Connected to cluster: %s\n",
				metadata.getClusterName());
		for (Host host : metadata.getAllHosts()) {
			System.out.printf("Datacenter: %s; Host: %s; Rack: %s\n",
					host.getDatacenter(), host.getAddress(), host.getRack());
		}
		session = cluster.connect();
	}

	private void createSchema() {
		session.execute("CREATE KEYSPACE IF NOT EXISTS hrdb WITH replication "
				+ "= {'class':'SimpleStrategy', 'replication_factor':3};");

		session.execute("CREATE TABLE IF NOT EXISTS hrdb.department ("
				+ "id text PRIMARY KEY," + "name text);");

		session.execute("CREATE TABLE IF NOT EXISTS hrdb.employee ("
				+ "id text PRIMARY KEY," + "department_id text,"
				+ "name text);");
		session.execute("CREATE INDEX employee_department_id ON hrdb.employee(department_id);");
	}

}
