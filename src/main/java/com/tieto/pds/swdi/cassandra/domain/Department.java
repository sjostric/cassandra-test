package com.tieto.pds.swdi.cassandra.domain;

import java.util.List;

public class Department {

	String id;

	String name;

	List<Employee> employees;

	public List<Employee> getEmployees() {
		return employees;
	}

	public void setEmployees(List<Employee> employees) {
		this.employees = employees;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		StringBuilder repr = new StringBuilder();
		repr.append("Department: " + name + "\n");
		for (Employee emp : employees) {
			repr.append("\t" + emp.toString() + "\n");
		}
		return repr.toString();
	}

}
