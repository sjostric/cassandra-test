package com.tieto.pds.swdi.cassandra.domain;

public class Employee {

	String id;
	String name;
	Department department;

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Name: " + name + ", Department: " + department.getName()
				+ ", id: " + id;

	}

}
