package com.tieto.pds.swdi.cassandra.repository;

import java.util.ArrayList;
import java.util.List;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.tieto.pds.swdi.cassandra.domain.Department;
import com.tieto.pds.swdi.cassandra.domain.Employee;

public class DepartmentRepository {

	private Session session;

	public DepartmentRepository(Cluster cluster) {
		this(cluster.connect());
	}

	public DepartmentRepository(Session session) {
		this.session = session;
	}

	public List<Department> getDepartments() {
		List<Department> departments = new ArrayList<Department>();
		ResultSet results = session.execute("SELECT * FROM hrdb.department;");
		for (Row row : results) {
			Department department = new Department();
			department.setId(row.getString("id"));
			department.setName(row.getString("name"));
			department.setEmployees(getEmployees(department));
			departments.add(department);
		}
		return departments;
	}

	public List<Employee> getEmployees(Department department) {
		ArrayList<Employee> employees = new ArrayList<Employee>();
		ResultSet results = session
				.execute("SELECT * FROM hrdb.employee WHERE department_id='"
						+ department.getId() + "';");
		for (Row row : results) {
			Employee employee = new Employee();
			employee.setId(row.getString("id"));
			employee.setName("name");
			employee.setDepartment(department);
			employees.add(employee);
		}
		return employees;
	}

}
